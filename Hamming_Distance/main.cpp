#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <utility>
#include "Graph.h"
int main() {
    std::ios::sync_with_stdio(false);
    std::string str;
    std::string pattern;
    std::cin>>str;
    std::cin>>pattern;
    auto newStrings = replaceQuestions(pattern, str);
    std::cout<<countHammingDistance(newStrings.first, newStrings.second)<<"\n";
    std::cout<<newStrings.second<<"\n";
    std::cout<<newStrings.first<<"\n";
    return 0;
}
