//
// Created by liza on 12.11.16.
//
#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <utility>

#ifndef HAMMING_DISTANCE_GRAPH_H
#define HAMMING_DISTANCE_GRAPH_H

struct AdjListElement {
    size_t vertex;
    int edgeNum;
    AdjListElement(size_t vertex_, int edgeNum_ ) :
            vertex(vertex_), edgeNum(edgeNum_) {}
};

class Graph {
private:
    std::vector<std::vector<AdjListElement>> adjacencyList_;
    std::vector<std::vector<int> > flow_;
    std::vector<std::vector<int> > capacity_;
    size_t numVertices_;
    size_t source_;
    size_t target_;
    bool TryReach(size_t start, size_t target, std::vector<size_t> &parent) ;
public:
    const static int INF = 1000000000; // 1e9
    void addEdge( size_t vertex1, size_t vertex2, int& edgeNum);
    Graph(const std::string& pattern, const std::string& str );
    size_t find_max_flow() ;
    std::vector<size_t>getZeros();
};
std::pair<std::string, std::string>replaceQuestions(const std::string& pattern, const std::string& str );

int countHammingDistanceEqual(const std::string& s1, const std::string& s2);

int countHammingDistance(const std::string& pattern, const std::string& str);

#endif //HAMMING_DISTANCE_GRAPH_H
