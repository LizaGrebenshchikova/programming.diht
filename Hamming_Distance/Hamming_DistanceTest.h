//
// Created by liza on 13.11.16.
//
#include <gtest/gtest.h>
#include <vector>
#include <string>
#include "Graph.h"

#ifndef HAMMING_DISTANCE_HAMMING_DISTANCETEST_H
#define HAMMING_DISTANCE_HAMMING_DISTANCETEST_H
TEST (CountHAmmingDistanceTest, Test_1) {
    std::string str = "00101000";
    std::string pattern = "100101";
    // act
    int answer;
    answer = countHammingDistance(pattern, str);
    // assert
    ASSERT_EQ(answer, 11);
}

TEST (CountHAmmingDistanceTest, Test_2) {
    std::string str = "01010101001";
    std::string pattern = "1010100";
    int answer;
    //act
    answer = countHammingDistance(pattern, str);
    // assert
    ASSERT_EQ(answer, 19);
}

TEST (ReplaceQuestionsTest, Replace_1) {
    std::string str = "00?";
    std::string pattern ="1?";
    std::string tmp1 = "000";
    std::string tmp2 = "10";
    std::pair<std::string,std::string>answer = std::make_pair(std::move(tmp2),std::move(tmp1));
    // act
     auto result = replaceQuestions(pattern, str);
    // assert
    ASSERT_EQ(result, answer);
}

#endif //HAMMING_DISTANCE_HAMMING_DISTANCETEST_H
