//
// Created by liza on 12.11.16.
//
#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <utility>
#include "Graph.h"

bool Graph::TryReach(size_t start, size_t target, std::vector<size_t> &parent) {
        std::queue<size_t> vertexQueue;
        std::vector<size_t> dist(numVertices_, Graph::INF);
        dist[start] = 0;
        vertexQueue.push(start);
        while (!vertexQueue.empty()) {
            size_t curV = vertexQueue.front();
            vertexQueue.pop();

            for (auto& cur_edge: adjacencyList_[curV]) {
                size_t nextV = cur_edge.vertex;
                if (nextV == curV) continue;

                if ((dist[nextV] > dist[curV] + 1) && (capacity_[curV][nextV]) > 0) {
                    dist[nextV] = dist[curV] + 1;
                    parent[nextV] = curV;
                    if (nextV == target) {
                        return true;
                    }
                    vertexQueue.push(nextV);
                }
            }
        }
        return false;
    }

void Graph::addEdge( size_t vertex1, size_t vertex2, int& edgeNum){
        adjacencyList_[vertex1].emplace_back(vertex2, edgeNum);
        ++edgeNum;
        adjacencyList_[vertex2].emplace_back(vertex1, edgeNum);
        ++edgeNum;
    }

Graph::Graph(const std::string& pattern, const std::string& str ) {
        numVertices_ = pattern.size() + str.size() + 2;
        adjacencyList_.resize(numVertices_);
        capacity_.resize(numVertices_, std::vector<int>(numVertices_, 0));
        flow_.resize(numVertices_, std::vector<int>(numVertices_, 0));
        source_ = 0;
        target_ = 1;
        int edgeNum = 0;
        for( size_t strIndex = 0; strIndex <= (str.size() - pattern.size()); ++strIndex) {
            for (size_t i = 0; i < pattern.size(); ++i) {
                if ((pattern[i] == '?') && (str[strIndex + i] != '?')) {
                    if(capacity_[2 + i][(int)str[strIndex + i] - (int)'0'] == 0) {
                        addEdge(2 + i, (int)str[strIndex + i] - (int)'0', edgeNum);
                    }
                    ++capacity_[2 + i][(int)str[strIndex + i] - (int)'0'] ;
                    ++capacity_[(int)str[strIndex + i] - (int)'0'][2 + i];
                }
                if ((pattern[i] != '?') && (str[strIndex + i] == '?')) {
                    if(capacity_[2 + pattern.size() + strIndex + i][(int)pattern[i] - (int)'0'] == 0) {
                        addEdge(2 + pattern.size() + strIndex + i, (int)pattern[i] - (int)'0', edgeNum);
                    }
                    ++capacity_[2 + pattern.size() + strIndex + i][(int)pattern[i] - (int)'0'];
                    ++capacity_[(int)pattern[i] - (int)'0'][2 + pattern.size() + strIndex + i];
                }
                if ((pattern[i] == '?') && (str[strIndex + i] == '?')) {
                    if(capacity_[2 + i][2 + pattern.size() + strIndex + i] == 0) {
                        addEdge(2 + i, 2 + pattern.size() + strIndex + i, edgeNum);
                    }
                    ++capacity_[2 + i][2 + pattern.size() + strIndex + i];
                    ++capacity_[2 + pattern.size() + strIndex + i][2 + i];
                }
            }

        }
    }

size_t Graph::find_max_flow() {
        if (numVertices_ == 0)
            return -1;

        int answer = 0;
        std::vector<size_t> parent(numVertices_, -1);

        while (TryReach(source_, target_, parent)) {
            int minCapacity = Graph::INF;
            size_t curV = target_;

            while (curV != source_) {
                size_t prevV = parent[curV];
                minCapacity = std::min(minCapacity, capacity_[prevV][curV]);
                curV = prevV;
            }
            curV = target_;
            while (curV != source_) {
                size_t prevV = parent[curV];
                capacity_[prevV][curV] -= minCapacity;
                flow_[prevV][curV] += minCapacity;
                capacity_[curV][prevV] += minCapacity;
                flow_[curV][prevV] -= minCapacity;
                curV = prevV;
            }

            parent.assign(numVertices_, -1);
            answer += minCapacity;
        }
        return answer;

}

std::vector<size_t> Graph::getZeros() {
        find_max_flow();
        std::vector<size_t> parent(numVertices_, -1);
        TryReach(source_, target_, parent);
        std::vector<size_t>answer;
        for (size_t curV = 0; curV < numVertices_; ++curV){
            if ( parent[curV] != -1) {
                answer.push_back(curV);
            }
        }
        return answer;
    }

std::pair<std::string, std::string>replaceQuestions(const std::string& pattern, const std::string& str ) {
    std::string newPattern(pattern);
    std::string newStr(str);
    Graph ourGraph(newPattern, newStr);
    std::vector<size_t>zeroes = ourGraph.getZeros();
    for(size_t i = 0; i < zeroes.size(); ++i){
        size_t posZero = zeroes[i] - 2;
        if (posZero < pattern.size()){
            newPattern[posZero] = '0';
        } else {
            newStr[posZero - pattern.size()] = '0';
        }
    }
    for (size_t j = 0; j < pattern.size(); ++j){
        if (newPattern[j] == '?') {
            newPattern[j] ='1';
        }
    }
    for (size_t k = 0; k < newStr.size(); ++k){
        if (newStr[k] == '?') {
            newStr[k] ='1';
        }
    }
    return std::make_pair(newPattern, newStr);
}

int countHammingDistanceEqual(const std::string& s1, const std::string& s2) {
    int answer = 0;
    for (size_t i = 0; i < s1.size(); ++i){
        if(s1[i] != s2[i]){
            ++answer;
        }
    }
    return answer;
}

int countHammingDistance(const std::string& pattern, const std::string& str) {
    int answer = 0;
    for (size_t i = 0; i <= (str.size() - pattern.size()); ++i ){
        std::string tmpStr(str.begin() + i, str.begin() + i + pattern.size());
        answer += countHammingDistanceEqual(pattern, tmpStr);
    }
    return answer;
}
