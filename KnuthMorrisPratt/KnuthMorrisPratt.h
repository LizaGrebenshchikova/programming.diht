//
// Created by liza on 06.12.16.
//
#include <iostream>
#include <algorithm>
#include <vector>

#ifndef KNUTHMORRISPRATT_KNUTHMORRISPRATT_H
#define KNUTHMORRISPRATT_KNUTHMORRISPRATT_H

std::vector<int> prefixFunction(const std::string &to_calculate) {
  std::vector<int> pi(to_calculate.length(), 0);
  int counter;
  for (int i = 1; i < to_calculate.length(); ++i) {
    counter = pi[i - 1];
    while (counter > 0 && to_calculate[counter] != to_calculate[i])
      counter = pi[counter - 1];
    if (to_calculate[counter] == to_calculate[i])
      ++counter;
    pi[i] = counter;
  }
  return pi;
}

void KnuthMorrisPratt(const std::string &pattern, const std::string &to_calculate) {
  std::vector<int> pi = prefixFunction(pattern);
  int counter = 0;
  for (int i = 0; i < to_calculate.length(); ++i) {
    while (counter > 0 && (counter >= pattern.length() || pattern[counter] != to_calculate[i]))
      counter = pi[counter - 1];
    if (pattern[counter] == to_calculate[i])
      ++counter;

    if (counter == pattern.length()) {
      std::cout << i - counter + 1 << " ";
    }
  }
}

#endif //KNUTHMORRISPRATT_KNUTHMORRISPRATT_H
