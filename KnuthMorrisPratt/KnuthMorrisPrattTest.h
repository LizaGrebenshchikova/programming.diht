//
// Created by liza on 06.12.16.
//
#include <gtest/gtest.h>
#include <vector>
#include "KnuthMorrisPratt.h"

#ifndef KNUTHMORRISPRATT_KNUTHMORRISPRATTTEST_H
#define KNUTHMORRISPRATT_KNUTHMORRISPRATTTEST_H

TEST (prefixFunctionTest, Test_1) {
  std::string examp_1 = "abaaaba";
  std::vector<int> answer{0, 0, 1, 1, 1, 2, 3};
  std::vector<int> result;
  // act
  result = prefixFunction(examp_1);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (prefixFunctionTest, Test_2) {
  std::string examp_2 = "abaaaabaaba";
  std::vector<int> answer{0, 0, 1, 1, 1, 1, 2, 3, 4, 2, 3};
  std::vector<int> result;
  // act
  result = prefixFunction(examp_2);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (prefixFunctionTest, Test_3) {
  std::string examp_3 = "abacab";
  std::vector<int> answer{0, 0, 1, 0, 1, 2};
  std::vector<int> result;
  // act
  result = prefixFunction(examp_3);
  // assert
  ASSERT_EQ(result, answer);
}

#endif //KNUTHMORRISPRATT_KNUTHMORRISPRATTTEST_H
