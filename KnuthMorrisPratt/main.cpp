
#include <iostream>
#include "KnuthMorrisPratt.h"

int main() {
  std::ios_base::sync_with_stdio(false);
  std::string pattern;
  std::string ourString;
  std::cin >> pattern >> ourString;
  KnuthMorrisPratt(pattern, ourString);
  return 0;
}