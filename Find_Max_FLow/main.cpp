#include <iostream>
#include <vector>
#include <queue>
#include <utility>
#include "Graph.h"

int main() {
    std::ios::sync_with_stdio(false);
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    bool flag = true;
    while (flag) {
        Graph our_graph;
        std::cin >> our_graph;
        int answer;
        answer = our_graph.find_max_flow();
        if (answer != -1) {
            std::cout << answer << "\n";
        } else {
            flag = false;
        }
    }
    fclose(stdin);
    fclose(stdout);
    return 0;
}