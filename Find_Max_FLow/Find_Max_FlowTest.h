//
// Created by liza on 12.11.16.
//
#include <gtest/gtest.h>
#include <vector>
#include "Graph.h"

#ifndef FIND_MAX_FLOW_FIND_MAX_FLOW_H
#define FIND_MAX_FLOW_FIND_MAX_FLOW_H

TEST (Find_max_flowTest, Find_1) {
    std::vector<std::vector<size_t >> capacity{{0, 20, 10, 0},
                                               {20, 0, 5, 10},
                                               {10, 5, 0, 20},
                                               {0, 10, 20, 0}};
    std::vector<std::vector<size_t >> flow{{0, 0, 0, 0},
                                           {0, 0, 0, 0},
                                           {0, 0, 0, 0},
                                           {0, 0, 0, 0}};
    size_t source = 1;
    size_t target = 4;
    int numVertices = 4;
    Graph our_graph(numVertices, source, target,std::move(flow), std::move(capacity));
    // act
    int answer;
    answer = our_graph.find_max_flow();
    // assert
    ASSERT_EQ(answer, 25);
}

TEST (Find_max_flowTest, Find_2) {
    std::vector<std::vector<size_t >> capacity{{0, 1, 0, 0, 1, 0},
                                                {1, 0, 2, 1, 2, 0},
                                                {0, 2, 0, 0, 0, 2},
                                                {0, 1, 0, 0, 0, 1},
                                                {1, 2, 0, 0, 0, 0},
                                                {0, 0, 2, 1, 0, 0}};

    std::vector<std::vector<size_t >> flow{{0, 1, 0, 0, 1, 0},
                                           {1, 0, 2, 1, 2, 0},
                                           {0, 2, 0, 0, 0, 2},
                                           {0, 1, 0, 0, 0, 1},
                                           {1, 2, 0, 0, 0, 0},
                                           {0, 0, 2, 1, 0, 0}};
    size_t source = 1;
    size_t target = 4;
    int numVertices = 6;
    Graph our_graph(numVertices, source, target,std::move(flow), std::move(capacity));
    // act
    int answer;
    answer = our_graph.find_max_flow();
    // assert
    ASSERT_EQ(answer, 2);
}

TEST (Find_max_flowTest, Find_3) {
    std::vector<std::vector<size_t >> capacity{{0, 1, 0, 0},
                                               {1, 0, 1, 1},
                                               {0, 1, 0, 1},
                                               {0, 1, 1, 0}};
    std::vector<std::vector<size_t >> flow{{0, 0, 0, 0},
                                           {0, 0, 0, 0},
                                           {0, 0, 0, 0},
                                           {0, 0, 0, 0}};
    size_t source = 1;
    size_t target = 4;
    int numVertices = 4;
    Graph our_graph(numVertices, source, target,std::move(flow), std::move(capacity));
    // act
    int answer;
    answer = our_graph.find_max_flow();
    // assert
    ASSERT_EQ(answer, 1);
}

#endif //FIND_MAX_FLOW_FIND_MAX_FLOW_H
