//
// Created by liza on 12.11.16.
//
#include <iostream>
#include <vector>
#include <queue>
#include <utility>
#include <limits>
#include "Graph.h"

bool Graph::TryReach(size_t start, size_t target, std::vector<size_t> &parent) {
    std::queue<size_t> vertexQueue;
    std::vector<size_t> dist(num_vertices_, std::numeric_limits<int>::max());
    dist[start] = 0;
    vertexQueue.push(start);
    while (!vertexQueue.empty()) {
        size_t curV = vertexQueue.front();
        vertexQueue.pop();

        for (size_t nextV = 0; nextV != num_vertices_; ++nextV) {
            if (nextV == curV) continue;

            if ((dist[nextV] > dist[curV] + 1) && (capacity_[curV][nextV]) > 0) {
                dist[nextV] = dist[curV] + 1;
                parent[nextV] = curV;
                if (nextV == target) {
                    return true;
                }
                vertexQueue.push(nextV);
            }
        }
    }
    return false;
}

Graph::Graph() :
        num_vertices_(0), source_(0), target_(0){}

Graph::Graph(size_t num_vertices, size_t source, size_t target, std::vector<std::vector<size_t>> &&flow,
             std::vector<std::vector<size_t>> &&capacity) : num_vertices_(num_vertices),
                                                            source_((source - 1)),
                                                            target_((target - 1)),
                                                            flow_(std::move(flow)),
                                                            capacity_(std::move(capacity)) {}

int Graph::find_max_flow() {
    if (num_vertices_ == 0)
        return -1;

    int answer = 0;
    std::vector<size_t> parent(num_vertices_, -1);

    while (TryReach(source_, target_, parent)) {
        size_t minCapacity = std::numeric_limits<int>::max();
        size_t curV = target_;

        while (curV != source_) {
            size_t prevV = parent[curV];
            minCapacity = std::min(minCapacity, capacity_[prevV][curV]);
            curV = prevV;
        }
        curV = target_;
        while (curV != source_) {
            size_t prevV = parent[curV];
            capacity_[prevV][curV] -= minCapacity;
            flow_[prevV][curV] += minCapacity;
            capacity_[curV][prevV] += minCapacity;
            flow_[curV][prevV] -= minCapacity;
            curV = prevV;
        }

        parent.assign(num_vertices_, -1);
        answer += minCapacity;
    }
    return answer;
}

std::istream &operator>>(std::istream &in, Graph &our_graph) {
    size_t curr_num_vertices;
    size_t c;

    in >> curr_num_vertices;
    if (curr_num_vertices == 0) {
        return in;
    }
    our_graph.num_vertices_ = curr_num_vertices;
    our_graph.flow_.resize(curr_num_vertices, std::vector<size_t>(curr_num_vertices, 0));
    our_graph.capacity_.resize(curr_num_vertices, std::vector<size_t>(curr_num_vertices, 0));
    in >> our_graph.source_;
    in >> our_graph.target_;
    in >> c;
    --our_graph.source_;
    --our_graph.target_;

    size_t vertex_1;
    size_t vertex_2;
    int curCapacity;

    for (int i = 0; i < c; ++i) {
        in >> vertex_1;
        in >> vertex_2;
        in >> curCapacity;

        --vertex_1;
        --vertex_2;

        our_graph.capacity_[vertex_1][vertex_2] += curCapacity;
        our_graph.capacity_[vertex_2][vertex_1] += curCapacity;
    }
    return in;
}
