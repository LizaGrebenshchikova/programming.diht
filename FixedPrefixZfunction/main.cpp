#include <iostream>
#include "PrefixZfunction.h"
int main() {
  std::string new_string;
  std::cout << "Enter the string\n";
  std::cin >> new_string;

  std::vector<int> z;
  std::vector<int> prefix;
  prefix = prefixFunction(new_string);
  z = zFunction(new_string);
  std::cout << "Prefix function :\n";
  for (int i = 0; i < prefix.size(); ++i) {
    std::cout << prefix[i] << " ";
  }
  std::cout << "\nZ function :\n";
  for (int i = 0; i < z.size(); ++i) {
    std::cout << z[i] << " ";
  }
  std::cout << "\n";

  std::vector<int> new_prefix;
  new_prefix = prefixFromZ(z);
  for (int i = 0; i < new_prefix.size(); ++i) {
    std::cout << new_prefix[i] << " ";
  }

  std::string calc_string;
  calc_string = stringFromPrefix(prefix);
  std::cout << "\n" << calc_string << "\n";

  return 0;
}