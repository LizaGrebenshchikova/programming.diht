//
// Created by liza on 06.12.16.
//
//
#include <iostream>
#include <string>
#include <vector>
#define ALPH_SIZE 26

#ifndef PREFIXZFUNCTION_PREFIXZFUNCTION_H
#define PREFIXZFUNCTION_PREFIXZFUNCTION_H

std::vector<int> zFunction(const std::string &to_calculate);
std::vector<int> prefixFunction(const std::string &to_calculate);

std::vector<int> prefixFromZ(const std::vector<int> &zFunction);

std::string stringFromPrefix(const std::vector<int> &prefix);

#endif //PREFIXZFUNCTION_PREFIXZFUNCTION_H
