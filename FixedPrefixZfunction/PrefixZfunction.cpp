//
// Created by liza on 06.12.16.
//
#include "PrefixZfunction.h"

std::vector<int> prefixFunction(const std::string &to_calculate) {
  std::vector<int> prefix(to_calculate.length(), 0);
  int counter = 0;
  for (int i = 1; i < to_calculate.length(); ++i) {
    counter = prefix[i - 1];
    while (counter > 0 && to_calculate[counter] != to_calculate[i])
      counter = prefix[counter - 1];
    if (to_calculate[counter] == to_calculate[i])
      ++counter;
    prefix[i] = counter;
  }
  return prefix;
}

std::vector<int> zFunction(const std::string &to_calculate) {
  std::vector<int> z;
  z.resize(to_calculate.length());
  int left = 0;
  int right = 0;
  for (int i = 1; i < to_calculate.length(); ++i) {
    z[i] = std::max(0, std::min(right - i, z[i - left]));
    while (i + z[i] < to_calculate.length() && to_calculate[z[i]] == to_calculate[i + z[i]]) {
      ++z[i];
    }
    if (i + z[i] >= right) {
      left = i;
      right = i + z[i];
    }
  }
  return z;
}

std::vector<int> prefixFromZ(const std::vector<int> &z) {
  std::vector<int> prefix(z.size(), 0);
  for (int i = 1; i < z.size(); ++i) {
    for (int j = z[i] - 1; j >= 0 && prefix[i + j] == 0; --j) {
      prefix[i + j] = j + 1;
    }
  }
  return prefix;
}

std::string stringFromPrefix(const std::vector<int> &prefix) {
  std::string new_string = "a";
  std::vector<bool> used(ALPH_SIZE, false);
  for (int i = 1; i < prefix.size(); ++i) {
    if (prefix[i] == 0) {
      int count = prefix[i - 1];
      while (count > 0) {
        used[new_string[count] - 'a'] = true;
        count = prefix[count - 1];
      }
      used[new_string[0] - 'a'] = true;
      int minChar = 0;
      while (used[minChar]) {
        ++minChar;
      }
      new_string += (minChar + 'a');
      used.assign(ALPH_SIZE, false);
    } else {
      new_string += new_string[prefix[i] - 1];
    }
  }
  return new_string;
}

