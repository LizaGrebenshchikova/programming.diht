//
// Created by liza on 06.12.16.
//
#include <gtest/gtest.h>
#include <vector>
#include "PrefixZfunction.h"

#ifndef PREFIXZFUNCTION_PREFIXZFUNCTIONTEST_H
#define PREFIXZFUNCTION_PREFIXZFUNCTIONTEST_H

TEST (zFunctionTest, Test_1) {
  std::string examp_1 = "aaaba";
  std::vector<int> answer{0, 2, 1, 0, 1};
  std::vector<int> result;
  // act
  result = zFunction(examp_1);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (prefixFunctionTest, Test_2) {
  std::string examp_1 = "aaaba";
  std::vector<int> answer{0, 1, 2, 0, 1};
  std::vector<int> result;
  // act
  result = prefixFunction(examp_1);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (prefixFromZTest, Test_3) {
  std::vector<int> answer{0, 1, 2, 0, 1};
  std::vector<int> z{0, 2, 1, 0, 1};
  std::vector<int> result;
  // act
  result = prefixFromZ(z);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (stringFromPrefixTest, Test_4) {
  std::vector<int> prefix{0, 1, 2, 0, 1};
  std::string result;
  std::string answer = "aaaba";
  // act
  result = stringFromPrefix(prefix);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (zFunctionTest, Test_5) {
  std::string examp_2 = "ababaaaab";
  std::vector<int> answer{0, 0, 3, 0, 1, 1, 1, 2, 0};
  std::vector<int> result;
  // act
  result = zFunction(examp_2);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (prefixFunctionTest, Test_6) {
  std::string examp_2 = "ababaaaab";
  std::vector<int> answer{0, 0, 1, 2, 3, 1, 1, 1, 2};
  std::vector<int> result;
  // act
  result = prefixFunction(examp_2);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (prefixFromZTest, Test_7) {
  std::vector<int> answer{0, 0, 1, 2, 3, 1, 1, 1, 2};
  std::vector<int> z{0, 0, 3, 0, 1, 1, 1, 2, 0};
  std::vector<int> result;
  // act
  result = prefixFromZ(z);
  // assert
  ASSERT_EQ(result, answer);
}
TEST (stringFromPrefixTest, Test_8) {
  std::vector<int> prefix{0, 0, 1, 2, 3, 1, 1, 1, 2};
  std::string result;
  std::string answer = "ababaaaab";
  // act
  result = stringFromPrefix(prefix);
  // assert
  ASSERT_EQ(result, answer);
}
#endif //PREFIXZFUNCTION_PREFIXZFUNCTIONTEST_H
