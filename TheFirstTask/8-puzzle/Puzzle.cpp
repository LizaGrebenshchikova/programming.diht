//
// Created by liza on 09.10.16.
//
#include "Puzzle.h"
#include <algorithm>
#include <iostream>
#include <cmath>

std::vector<int> FieldPosition::GetOurParent() const {
    return our_parent;
}
std::vector<int> FieldPosition::GetPosition() const {
    return position;
}
bool FieldPosition::operator<(const FieldPosition &need_to_compare) const {
    if(GetWeight() == need_to_compare.GetWeight()) {
        return this -> position > need_to_compare.position;
    }
    return GetWeight() < need_to_compare.GetWeight();
}
int FieldPosition::GetWeight()const {
    return this -> steps_num;
}

FieldPosition::FieldPosition(const std::vector<int> &need_to_create, int new_steps_num, const std::vector<int> &parent){
    position = need_to_create;
    steps_num = new_steps_num;
    our_parent = parent;
}

FieldPosition::FieldPosition(const std::vector<int> &need_to_create)  {
    position = need_to_create;
    steps_num = 0;
}


bool FieldPosition::IsWinPosition() const {
    for(int i = 0; i <  8; ++i) {
        if (position[i] != i+1)
            return false;

    }
        return true;
}

FieldPosition FieldPosition::MakeNewNeighbour(int where_is_null, int shift) const {
    std::vector<int> neighbour_position = this -> position;
    std::swap(neighbour_position[where_is_null], neighbour_position[where_is_null + shift]);
    FieldPosition neighbour = FieldPosition(neighbour_position, this -> steps_num + 1, this -> position);
    return neighbour;
}

std::vector<std::string> FieldPosition:: FindTheWay(const std::map<std::vector<int>, std::pair<int, std::vector<int> > > &position_map) const {
    std::vector<int> check = position;
    std::vector<std::string> our_way;
    if(steps_num == 0)
        return our_way;
    int counter = steps_num ;
    auto our_element = position_map.find(check);
    while (our_element->second.second.size() > 0) {
        int where_is_null_son = std::find(check.begin(), check.end(), 0) - check.begin();
        int where_is_null_parent = std::find((our_element -> second.second).begin(), (our_element -> second.second).end(), 0) - (our_element -> second.second).begin();
        if(where_is_null_parent - where_is_null_son == 3)
            our_way.push_back("U");
        else if(where_is_null_parent - where_is_null_son == -3)
            our_way.push_back("D");
        else if(where_is_null_parent - where_is_null_son == 1)
            our_way.push_back("L");
        else if(where_is_null_parent - where_is_null_son == -1)
            our_way.push_back("R");

        check = our_element -> second.second;
        --counter;
        our_element = position_map.find(check);
    }
    return our_way;
}

void FieldPosition::CheckNewPosition(const FieldPosition &minimal, std::set<FieldPosition> &opened, std::map<std::vector<int>, std::pair<int, std::vector<int>>> &position_map) const {
    auto our_element = position_map.find(position);
    if(our_element != position_map.end()) {
        int old_distance = our_element -> second.first;
        if(GetWeight() < old_distance) {
            FieldPosition to_delete = FieldPosition(position, our_element -> second.first, minimal.position);
            opened.erase(to_delete);
            opened.insert(*this);
            our_element -> second.first = GetWeight();
        }
    }
    else {
        opened.insert(*this);
        position_map.insert(std::make_pair(position, std::make_pair(GetWeight(), our_parent)));
    }
}

int FieldPosition:: WhereIsNull() const {
    return std::find(position.begin(), position.end(), 0) - position.begin();
}

bool FieldPosition::HasSolution() const{// the number of inversions and the distance of the null from the necessary place has to be compared by mod2
    std::vector<int>position = GetPosition();
    int inversions = 0; // the number of iversions
    for( int i = 0; i < size_of_field; ++i){ // count the number of the inversions without 0
        if (position[i] != 0){
            for ( int j = 0; j < i; ++j){
                if( position[j] > position[i] || (position[j]== 0))
                    ++inversions;

            }
        }

    }
    int where_is_null = WhereIsNull();
    int diff_of_col = abs((where_is_null % 3) - 2);
    int diff_of_row = abs((where_is_null / 3) - 2);
    inversions += diff_of_col;
    inversions += diff_of_row;
    return inversions%2 == 0 ;
}

void GameSolver::Run(const FieldPosition& start_position) {

    bool  is_solvable = start_position.HasSolution();
    if (!is_solvable){
        std::cout<<"-1";
        return;
    }

    opened_positions.insert(start_position);
    position_map.insert(std::make_pair(start_position.GetPosition(), std::make_pair(start_position.GetWeight(), start_position.GetOurParent())));
    std::vector<std::string> our_way;
    while (!opened_positions.empty()) {
        FieldPosition minimal = *opened_positions.begin();
        opened_positions.erase(opened_positions.begin());

        if(minimal.IsWinPosition()) {
            our_way = minimal.FindTheWay(position_map);
            break;
        }

        int where_is_null = minimal.WhereIsNull();

        if(where_is_null % 3 != 2) {
            FieldPosition neighbour = minimal.MakeNewNeighbour(where_is_null, 1);
            neighbour.CheckNewPosition(minimal, opened_positions, position_map);
        }

        if(where_is_null % 3 != 0) {
            FieldPosition neighbour = minimal.MakeNewNeighbour(where_is_null, -1);
            neighbour.CheckNewPosition(minimal, opened_positions, position_map);
        }

        if(where_is_null / 3 != 2) {
            FieldPosition neighbour = minimal.MakeNewNeighbour(where_is_null, 3);
            neighbour.CheckNewPosition(minimal, opened_positions, position_map);
        }

        if(where_is_null / 3 != 0) {
            FieldPosition neighbour = minimal.MakeNewNeighbour(where_is_null, -3);
            neighbour.CheckNewPosition(minimal, opened_positions, position_map);
        }
    }

    std::reverse(our_way.begin(), our_way.end());
    std::cout<<our_way.size()<<"\n";
    for(int i = 0; i < our_way.size(); ++i) {
        std::cout << our_way[i] ;
    }
}



