cmake_minimum_required(VERSION 3.6)
project(8_puzzle)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES Puzzle.h Puzzle.cpp)
set(SOURCE_FILES_TEST Puzzle.h Puzzle.cpp 8-puzzleTest.h 8-puzzleTest.cpp)
add_executable(8_puzzle main.cpp ${SOURCE_FILES})


# Locate GTest
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

# Link runTests with what we want to test and the GTest and pthread library
add_executable(runTests 8-puzzleTest.cpp ${SOURCE_FILES_TEST})
target_link_libraries(runTests ${GTEST_LIBRARIES} pthread)