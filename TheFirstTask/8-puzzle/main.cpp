#include <iostream>
#include <vector>
#include "Puzzle.h"
int main() {

    /*std::vector<int> start_position;

    for (int i = 0; i < size_of_field; ++i) {
        int our_number;
        std::cin >> our_number;
        start_position.push_back(our_number);
    }

    FieldPosition StartField = FieldPosition(start_position);
    GameSolver Solution;
    Solution.Run(StartField);
     */
    std::vector<int> start_position1{1,2,3,4,5,6,7,8,0};
    std::vector<int> start_position2{1,2,4,3,6,5,7,8,0};
    std::vector<int> start_position3{2,3,1,4,5,6,7,8,0};
    std::vector<int> start_position4{1,2,3,6,5,4,0,8,7};
    FieldPosition Field1= FieldPosition(start_position1);
    FieldPosition Field2= FieldPosition(start_position2);
    FieldPosition Field3= FieldPosition(start_position3);
    FieldPosition Field4= FieldPosition(start_position4);
    bool flag1;
    bool flag2;
    flag1 = Field1 < Field2;
    flag2 = Field3 < Field4;
    std::cout<< flag1<<" "<<flag2;

    return 0;
}