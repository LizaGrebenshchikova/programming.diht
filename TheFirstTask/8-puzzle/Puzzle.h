//
// Created by liza on 09.10.16.
//
#include <vector>
#include <cstring>
#include <map>
#include <set>
#include <algorithm>
#ifndef INC_8_PUZZLE_PUZZLE_H
#define INC_8_PUZZLE_PUZZLE_H
const int size_of_field = 9;

class FieldPosition { // This is a class of our position in the game
private:
    std::vector<int> position; // Our current position
    int steps_num; //  The number of steps
    std::vector<int> our_parent; // The parent's situation
public:
    std::vector<int> GetOurParent() const;
    std::vector<int> GetPosition() const;
    bool operator <(const FieldPosition &ToCompare) const;
    int GetWeight() const;
    explicit FieldPosition(const std::vector<int> &need_to_create, int new_steps_num, const std::vector<int> &parent);
    explicit FieldPosition(const std::vector<int> &need_to_create);
    bool IsWinPosition() const;
    FieldPosition MakeNewNeighbour( int where_is_null, int shift) const;
    std::vector<std::string> FindTheWay(const std::map<std::vector<int>, std::pair<int, std::vector<int> > > &position_map) const;
    void CheckNewPosition(const FieldPosition &minimal, std::set<FieldPosition> &opened, std::map<std::vector<int>, std::pair<int, std::vector<int> > > &position_map) const;
    int WhereIsNull() const;
    bool HasSolution() const;
};
class GameSolver{
private:
    std::vector<std::string> our_way;//The steps sequence to solve the puzzle
    std::set<FieldPosition> opened_positions;//The set of the investigated positions
    std::map<std::vector<int>, std::pair<int, std::vector<int> > > position_map; //a map comprising the investigated positions and their parents
public:
    void Run(const FieldPosition& start_position);//the function of the solution of our puzzle
};

#endif //INC_8_PUZZLE_PUZZLE_H

