//
// Created by liza on 23.10.16.
//

#include <gtest/gtest.h>
#include "Puzzle.h"
#include <vector>

#ifndef INC_8_PUZZLE_8_PUZZLETEST_H
#define INC_8_PUZZLE_8_PUZZLETEST_H

TEST (HasSollutionTest, Has) {
    std::vector<int>start_position{1, 2, 4, 3, 5, 6, 8, 7, 0};
    FieldPosition StartField = FieldPosition(start_position);
    // act
    bool res;
    res = StartField.HasSolution();
    // assert
    ASSERT_EQ(res, true);

}

TEST (HasSollutionTest, HasNot) {
    std::vector<int>start_position{1, 3, 2, 4, 5, 6, 7, 8, 0};
    FieldPosition StartField = FieldPosition(start_position);
    // act
    bool res;
    res = StartField.HasSolution();
    // assert
    ASSERT_EQ(res, false);

}

TEST (OperatorLessTest, NotLess) {
    std::vector<int> position1{1,2,3,4,5,6,7,8,0};
    std::vector<int> position2{1,2,4,3,6,5,7,8,0};

    FieldPosition Field1= FieldPosition(position1);
    FieldPosition Field2= FieldPosition(position2);
    //act
    bool res;
    res = Field1 < Field2;
    //assert
    ASSERT_EQ(res, false);
}

TEST (OperatorLessTest, Less) {
    std::vector<int> position3{2,3,1,4,5,6,7,8,0};
    std::vector<int> position4{1,2,3,6,5,4,0,8,7};

    FieldPosition Field3= FieldPosition(position3);
    FieldPosition Field4= FieldPosition(position4);
    //act
    bool res;
    res = Field3 < Field4;
    //assert
    ASSERT_EQ(res, true);
}

#endif //INC_8_PUZZLE_8_PUZZLETEST_H
