//
// Created by Elizaveta Grebenshchikova on 06.10.16.
//

#include <vector>
#include <cstdio>
#include <iosfwd>
#include <iostream>

#ifndef ARBITRAGE_GRAPH_H
#define ARBITRAGE_GRAPH_H


class Graph {
private:
    const static int noEdge = -1;
    size_t num_vertices;
    std::vector<std::vector<float> > sales;
public:
    Graph();
    Graph(size_t);
    Graph(std::vector<std::vector<float>> && sales);
    size_t size() const;
    friend std::istream& operator>> (std::istream& in, Graph& our_graph);
    bool Find_Arbitrage() const ;
};


#endif //ARBITRAGE_GRAPH_H
