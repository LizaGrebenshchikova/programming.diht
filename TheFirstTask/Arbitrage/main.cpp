#include <iostream>

#include "Graph.h"

int main() {
    Graph our_graph;
    std::cin >> our_graph;
    bool answer = our_graph.Find_Arbitrage();
    if (answer) {
        std::cout<<"YES";
    } else {
        std::cout<<"NO";
    }
    return 0;
}