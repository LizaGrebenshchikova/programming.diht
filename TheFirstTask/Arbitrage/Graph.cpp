//
// Created by liza on 06.10.16.
//

#include "Graph.h"

Graph::Graph() :
    num_vertices(0)
{}

Graph::Graph(size_t num_vertices_init) :
    num_vertices(num_vertices_init)
{}

size_t Graph::size() const {
    return num_vertices;
}
Graph::Graph(std::vector<std::vector<float>> &&sales) :
        num_vertices(sales.size()), sales(std::move(sales)) {}

std::istream& operator>> (std::istream& in, Graph& our_graph) {
    size_t graph_size;
    in >> graph_size;
    our_graph.num_vertices = graph_size;
    our_graph.sales.resize(graph_size, std::vector<float> (graph_size, our_graph.noEdge));
    for(int i = 0; i < our_graph.size(); ++i) {
        for (int j = 0; j < our_graph.size(); ++j) {
            if (i != j) {
                in >> our_graph.sales[i][j];
            } else {
                our_graph.sales[i][j] = 1;
            }
        }
    }
    return in;
}

bool Graph::Find_Arbitrage() const {
    std::vector<float> path(num_vertices, -1);
    for (int start_v = 0; start_v < num_vertices; ++start_v){
        path.assign(num_vertices, -1);
        path[start_v] = 1;

        for(int k = 1; k < num_vertices; ++k) {
            for (int v = 0; v < num_vertices; ++v) {
                for (int to = 0; to < num_vertices; ++to) {
                    if (sales[v][to] != noEdge && (path[v] * sales[v][to] > path[to])) {
                        path[to] = path[v] * sales[v][to];
                    }
                }
            }
        }

        for (int v = 0; v < num_vertices; ++v) {
            for (int to = 0; to < num_vertices; ++to) {
                if (sales[v][to] != noEdge && (path[v] * sales[v][to] > path[to])) {
                    return true;
                }
            }
        }
        return false;
    }
    return true;
}
