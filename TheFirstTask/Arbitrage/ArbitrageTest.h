//
// Created by liza on 23.10.16.
//
#include <gtest/gtest.h>
#include "Graph.h"
#include <vector>

#ifndef ARBITRAGE_ARBITRAGETEST_H
#define ARBITRAGE_ARBITRAGETEST_H

TEST (FindArbitrageTest, Found) {
    std::vector<std::vector<float>> our_sales{{1, 32.1, 1.50, 78.66},
                                     {0.03, 1, 0.04, 2.43},
                                     {0.67, 21.22, 1, 51.89},
                                     {0.01, -1, 0.02, 1}};
    Graph Our_Graph(std::move(our_sales));

    // act
    bool res;
    res = Our_Graph.Find_Arbitrage();
    // assert
    ASSERT_EQ(res, true);
}
TEST (FindArbitrageTest, NotFound) {
    std::vector<std::vector<float>> our_sales{{1, 0.3},
                                              {0.2, 1}};
    Graph Our_Graph(std::move(our_sales));

    // act
    bool res;
    res = Our_Graph.Find_Arbitrage();
    // assert
    ASSERT_EQ(res, false);
}

#endif //ARBITRAGE_ARBITRAGETEST_H
