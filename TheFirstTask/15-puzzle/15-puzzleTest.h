//
// Created by liza on 23.10.16.
//

#include <gtest/gtest.h>
#include "Puzzle.h"
#include <vector>

#ifndef INC_15_PUZZLE_15_PUZZLETEST_H
#define INC_15_PUZZLE_15_PUZZLETEST_H

 TEST (HasSollutionTest, HasNot) {
    std::vector<int>start_position{1, 2, 3, 4, 5, 6, 7, 9, 8, 10, 11, 12, 13, 14, 0, 15};
    FieldPosition StartField = FieldPosition(start_position);
    // act
    bool res;
    res = StartField.HasSolution();
    // assert
    ASSERT_EQ(res, false);

}

 TEST (HasSollutionTest, Has) {
    std::vector<int>start_position{1, 3, 2, 4, 5, 6, 7, 8, 10, 9, 11, 12, 13, 14, 0, 15};
    FieldPosition StartField = FieldPosition(start_position);
    // act
    bool res;
    res = StartField.HasSolution();
    // assert
    ASSERT_EQ(res, true);

 }

 TEST (CountDistanceTest, dist1) {
    std::vector<int>start_position{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 15};
    FieldPosition StartField = FieldPosition(start_position);
    //act
    int res;
    res = StartField.CountDistance();
    //assert
    ASSERT_EQ(res, 2);
 }

 TEST (CountDistanceTest, dist2) {
    std::vector<int>start_position{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0};
    FieldPosition StartField = FieldPosition(start_position);
    //act
    int res;
    res = StartField.CountDistance();
    //assert
    ASSERT_EQ(res, 0);
 }

#endif //INC_15_PUZZLE_15_PUZZLETEST_H
