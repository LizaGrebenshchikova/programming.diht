//
// Created by liza on 09.10.16.
//
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "Graph.h"

Graph::Graph() :
        num_vertices(0)
{}

Graph::Graph(size_t num_vertices_init) :
        num_vertices(num_vertices_init)
{}

size_t Graph::size() const {
    return num_vertices;
}

Graph::Graph(std::vector<std::vector<float>> &&adj_matrix) :
        num_vertices(adj_matrix.size()), adj_matrix(std::move(adj_matrix)) {}

std::istream& operator>> (std::istream& in, Graph& our_graph) {
    size_t graph_size;
    in >> graph_size;
    our_graph.num_vertices = graph_size;
    our_graph.adj_matrix.resize(graph_size, std::vector<float> (graph_size));
    for(int i = 0; i < our_graph.size(); ++i) {
        for (int j = 0; j < our_graph.size(); ++j) {
            in >> our_graph.adj_matrix[i][j];
        }
    }

    return in;
}

std::vector<std::vector<float>> Graph::estimated_distances() const {
    std::vector<std::vector<float>> dist_matrix = adj_matrix;
    for ( int k = 0; k < size(); ++k) {
        for (int i = 0; i < size(); ++i) {
            for (int j = 0; j < size(); ++j){
                dist_matrix[i][j] = std::min(dist_matrix[i][j], dist_matrix[i][k] + dist_matrix[k][j]);
            }
        }
    }
    return dist_matrix;
}
