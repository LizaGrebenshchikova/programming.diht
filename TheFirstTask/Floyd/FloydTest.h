//
// Created by liza on 23.10.16.
//
#include <gtest/gtest.h>
#include "Graph.h"
#include <vector>

#ifndef FLOYD_FLOYDTEST_H
#define FLOYD_FLOYDTEST_H

TEST (EstimateDistancesTest, Estimate) {
    std::vector<std::vector<float>> adj_matrix{{0, 5, 9, 100},
                                              {100, 0, 2, 8},
                                              {100, 100, 0, 7},
                                              {4, 100, 100, 0}};
    Graph Our_Graph(std::move(adj_matrix));
    std::vector<std::vector<float>> answer{{0, 5, 7, 13},
                                              {12, 0, 2, 8},
                                              {11, 16, 0, 7},
                                              {4, 9, 11, 0}};


    // act
    std::vector<std::vector<float>>res;
    res = Our_Graph.estimated_distances();
    // assert
    ASSERT_EQ(res, answer);
}

#endif //FLOYD_FLOYDTEST_H
