//
// Created by liza on 09.10.16.
//
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#ifndef FLOYD_GRAPH_H
#define FLOYD_GRAPH_H
class Graph {
private:
    size_t num_vertices;
    std::vector<std::vector<float> > adj_matrix;
public:
    Graph();
    Graph(size_t);
    Graph(std::vector<std::vector<float>> && adj_matrix);
    size_t size() const;

    friend std::istream &operator>> (std::istream &in, Graph &our_graph);

    std::vector<std::vector<float>> estimated_distances() const;
};

#endif //FLOYD_GRAPH_H
