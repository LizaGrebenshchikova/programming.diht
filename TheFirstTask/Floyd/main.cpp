
#include "Graph.h"
int main(){
    Graph our_graph;
    std::cin>>our_graph;
    std::vector<std::vector<float>> dist_matrix;
    dist_matrix = our_graph.estimated_distances();

    for(size_t i = 0 ; i < dist_matrix.size(); ++i){
        for (size_t j = 0; j < dist_matrix.size(); ++j){
            std::cout<<dist_matrix[i][j]<<" ";
        }
        std::cout<<"\n";
    }
    return 0;
}