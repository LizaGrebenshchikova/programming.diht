#include <gtest/gtest.h>
#include <vector>
#include <tuple>
#include "Graph.h"

#ifndef MST_MST_TEST_H
#define MST_MST_TEST_H
TEST (FindMSTTest, MST_1) {
  std::vector<Edge> edges;
  edges.emplace_back(0, 2, 1);
  edges.emplace_back(1, 2, 2);
  edges.emplace_back(2, 3, 1);
  edges.emplace_back(1, 3, 1);
  Graph our_graph(edges, 4);
  std::vector<Edge> answer;
  answer.emplace_back(0, 2, 1);
  answer.emplace_back(2, 3, 1);
  answer.emplace_back(1, 3, 1);
  // act
  std::vector<Edge> MST = our_graph.findMST();
  //assert
  ASSERT_EQ(MST, answer);
}

TEST (FindMSTTest, MST_2) {
  std::vector<Edge> edges;
  edges.emplace_back(0, 1, 1);
  edges.emplace_back(0, 2, 4);
  edges.emplace_back(0, 3, 5);
  edges.emplace_back(1, 2, 3);
  edges.emplace_back(1, 3, 2);
  edges.emplace_back(2, 3, 7);
  edges.emplace_back(3, 4, 6);
  Graph our_graph(edges, 5);
  std::vector<Edge> answer;
  answer.emplace_back(0, 1, 1);
  answer.emplace_back(1, 3, 2);
  answer.emplace_back(1, 2, 3);
  answer.emplace_back(3, 4, 6);
  // act
  std::vector<Edge> MST = our_graph.findMST();
  // assert
  ASSERT_EQ(MST.size(), 4);
  ASSERT_EQ(MST, answer);
}
TEST (CountWeightMSTTest, weight1) {
  std::vector<Edge> edges(7);
  edges.emplace_back(1, 2, 1);
  edges.emplace_back(1, 3, 4);
  edges.emplace_back(1, 4, 5);
  edges.emplace_back(2, 3, 3);
  edges.emplace_back(2, 4, 2);
  edges.emplace_back(3, 4, 7);
  edges.emplace_back(4, 5, 6);
  Graph our_graph(edges, 7);
  std::vector<Edge> MST = our_graph.findMST();
  // act
  int64_t answer;
  answer = Graph::countWeightMST(MST);
  // assert
  ASSERT_EQ(answer, 12);
}

#endif //MST_MST_TEST_H
