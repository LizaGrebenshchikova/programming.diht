#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>
#include <fstream>

#ifndef MST_GRAPH_H
#define MST_GRAPH_H

struct Edge {
  long long int weight_;
  int v1_;
  int v2_;

  Edge(int v1 = 0, int v2 = 0, int64_t weight = 0) {
    v1_ = v1;
    v2_ = v2;
    weight_ = weight;
  }

  bool operator<(const Edge &other) const {
    return weight_ < other.weight_;
  }

  bool operator==(const Edge &other) const {
    return weight_ == other.weight_ &&
        ((v1_ == other.v1_ && v2_ == other.v2_)
            || (v1_ == other.v2_ && v2_ == other.v1_));
  }
};

class Graph {
 private:
  size_t num_vertices_; // the number of num_vertices_ in the graph
  std::vector<Edge>
      edges; // vector with the triples: the first vertex, the second vertex, the weight of the edge between the num_vertices_
 public:
  Graph();
  Graph(const std::vector<Edge> &new_edges, size_t cur_vertice_num);
  size_t size() const;
  friend std::istream &operator>>(std::istream &in, Graph &our_graph);
  std::vector<Edge> findMST();
  static int64_t countWeightMST(const std::vector<Edge> &MST);
};

#endif //MST_GRAPH_H
