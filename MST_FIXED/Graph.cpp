#include <iostream>
#include <vector>
#include <algorithm>
#include "Graph.h"
#include "DSU.h"

Graph::Graph() : num_vertices_(0) {}

Graph::Graph(const std::vector<Edge> &new_edges, size_t cur_vertice_num)
    : edges(new_edges),
      num_vertices_(cur_vertice_num) {}

size_t Graph::size() const {
  return num_vertices_;
}

std::istream &operator>>(std::istream &in, Graph &our_graph) {
  int edges;
  int vertex1;
  int vertex2;
  long long int weight;

  in >> our_graph.num_vertices_;
  in >> edges;

  for (int i = 0; i < edges; ++i) {
    in >> vertex1;
    in >> vertex2;
    in >> weight;
    --vertex1;
    --vertex2;
    our_graph.edges.emplace_back(vertex1, vertex2, weight);
  }

  return in;
}

std::vector<Edge> Graph::findMST() {
  long long int weight;
  int vertex1;
  int vertex2;

  std::vector<int> vertices(num_vertices_);
  for (size_t i = 0; i < num_vertices_; ++i) {
    vertices[i] = static_cast<int>(i);
  }
  DSU<int> dsu(vertices);

  std::sort(edges.begin(), edges.end()); // sort our edges by weight
  std::vector<Edge> MST; // the result

  for (int i = 0; i < edges.size(); ++i) {
    weight = edges[i].weight_;
    vertex1 = edges[i].v1_;
    vertex2 = edges[i].v2_;
    if (dsu.find(vertex1) != dsu.find(vertex2)) { // because we don't want to have loops
      MST.emplace_back(vertex1, vertex2, weight);
      dsu.merge(vertex1, vertex2);
    }
  }
  return MST;
}

int64_t Graph::countWeightMST(const std::vector<Edge> &MST) {
  size_t res = 0;
  for (int i = 0; i < MST.size(); ++i) {
    res = res + MST[i].weight_;
  }
  return res;
}
