//
// Created by liza on 11.12.16.
//

#ifndef MST_DSU_H
#define MST_DSU_H

#include <cstdio>
#include <vector>
#include <map>
#include <cassert>

template<typename T>
class DSU {
 public:
  DSU() : size_(0) {}

  DSU(const std::vector<T> &elems) : size_(elems.size()) {
    for (size_t i = 0; i < size_; i++) {
      parent_[elems[i]] = elems[i];
      rank_[elems[i]] = 1;
    }
  }
  int find(const T &elem) {
    if (parent_[elem] == elem) {
      return elem;
    }
    return parent_[elem] = find(parent_[elem]);
  }

  void merge(const T &elem1, const T &elem2) {
    if (rank_[elem1] > rank_[elem2]) {
      parent_[find(elem2)] = find(elem1);
      rank_[find(elem1)] += rank_[find(elem2)];
    } else {
      parent_[find(elem1)] = find(elem2);
      rank_[find(elem2)] += rank_[find(elem1)];
    }
  }

 private:
  size_t size_;
  std::map<T, T> parent_;
  std::map<T, int> rank_;
};

#endif //MST_DSU_H
