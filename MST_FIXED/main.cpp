#include "Graph.h"

int main() {
  freopen("kruskal.in", "r", stdin);
  freopen("kruskal.out", "w", stdout);
  Graph ourGraph;
  std::cin >> ourGraph;

  std::vector<Edge> MST = ourGraph.findMST();
  int64_t answer;
  answer = Graph::countWeightMST(MST);
  std::cout << answer;
  fclose(stdin);
  fclose(stdout);
  return 0;
}