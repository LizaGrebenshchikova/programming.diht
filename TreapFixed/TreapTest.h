//
// Created by liza on 10.12.16.
//
#include <gtest/gtest.h>
#include <vector>
#include "Treap.h"
#ifndef TREAP_TREAPTEST_H
#define TREAP_TREAPTEST_H

TEST (insertTest, Test_1) {
  std::shared_ptr<Treap_node> global_root(nullptr);
  int number_of_nodes = 5;
  std::vector<int> keys{7, 4, 2, 6, 13};
  std::vector<int> priorities{10, 6, 4, 2, 8};
  for (int i = 0; i < number_of_nodes; ++i) {
    insert(global_root, std::make_shared<Treap_node>(keys.back(), priorities.back()));
    keys.pop_back();
    priorities.pop_back();
  }
  int answer_key = 7;
  int answer_priority = 10;
  int result_key = global_root->get_key();
  int result_priority = global_root->get_priority();
  // assert
  ASSERT_EQ(result_key, answer_key);
  ASSERT_EQ(result_priority, answer_priority);
}
TEST (insertTest, Test_2) {
  std::shared_ptr<Treap_node> global_root(nullptr);
  int number_of_nodes = 5;
  std::vector<int> keys{8, 5, 3, 7, 14};
  std::vector<int> priorities{11, 7, 5, 3, 9};
  for (int i = 0; i < number_of_nodes; ++i) {
    insert(global_root, std::make_shared<Treap_node>(keys.back(), priorities.back()));
    keys.pop_back();
    priorities.pop_back();
  }
  int answer_key = 8;
  int answer_priority = 11;
  int result_key = global_root->get_key();
  int result_priority = global_root->get_priority();
  // assert
  ASSERT_EQ(result_key, answer_key);
  ASSERT_EQ(result_priority, answer_priority);
}
TEST (insertTest, Test_3) {
  std::shared_ptr<Treap_node> global_root(nullptr);
  int number_of_nodes = 5;
  std::vector<int> keys{5, 3, 1, 4, 2};
  std::vector<int> priorities{8, 7, 4, 5, 1};
  for (int i = 0; i < number_of_nodes; ++i) {
    insert(global_root, std::make_shared<Treap_node>(keys.back(), priorities.back()));
    keys.pop_back();
    priorities.pop_back();
  }
  int answer_key = 5;
  int answer_priority = 8;
  int result_key = global_root->get_key();
  int result_priority = global_root->get_priority();
  // assert
  ASSERT_EQ(result_key, answer_key);
  ASSERT_EQ(result_priority, answer_priority);
}

#endif //TREAP_TREAPTEST_H
