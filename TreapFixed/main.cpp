#include <iostream>
#include "Treap.h"

int main() {
  std::shared_ptr<Treap_node> our_global_root(nullptr);
  std::shared_ptr<Treap_node> our_global_false_root(nullptr);

  int number_of_nodes;
  std::cin >> number_of_nodes;

  std::vector<int> keys;
  std::vector<int> first_priorities;
  std::vector<int> false_priorities;
  for (int i = 0; i < number_of_nodes; ++i) {
    int cur_key, cur_priority;
    std::cin >> cur_key >> cur_priority;

    keys.push_back(cur_key);
    first_priorities.push_back(cur_priority);
    false_priorities.push_back(number_of_nodes - i);

    insert(our_global_root, std::make_shared<Treap_node>(keys.back(), first_priorities.back()));
    insert(our_global_false_root, std::make_shared<Treap_node>(keys.back(), false_priorities.back()));
  }
  std::cout << our_global_false_root->height() - our_global_root->height();
  return 0;
}