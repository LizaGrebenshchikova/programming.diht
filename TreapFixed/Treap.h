//
// Created by liza on 06.12.16.
//
#include <iostream>
#include <memory>
#include <algorithm>

#ifndef TREAP_TREAP_H
#define TREAP_TREAP_H

class Treap_node {
 private:
  int key_;
  int priority_;
  std::shared_ptr<Treap_node> left_node_;
  std::shared_ptr<Treap_node> right_node_;
 public:
  Treap_node() {};
  Treap_node(int key, int priority) : key_(key), priority_(priority),
                                      left_node_(nullptr), right_node_(nullptr) {};
  void traversal(std::ostream &outputStream = std::cout);
  const int height();
  const int get_key();
  const int get_priority();
  std::shared_ptr<Treap_node> &get_left_node();
  std::shared_ptr<Treap_node> &get_right_node();
};
void split(std::shared_ptr<Treap_node> root,
           const std::shared_ptr<Treap_node> split_node,
           std::shared_ptr<Treap_node> &left,
           std::shared_ptr<Treap_node> &right);
void insert(std::shared_ptr<Treap_node> &root, std::shared_ptr<Treap_node> to_insert);
#endif //TREAP_TREAP_H
