//
// Created by liza on 06.12.16.
//
#include "Treap.h"

void Treap_node::traversal(std::ostream &outputStream) {
  outputStream << key_ << "  " << priority_ << "\n";
  if (left_node_) {
    outputStream << "left\n";
    left_node_->traversal();
  }

  if (right_node_) {
    outputStream << "right\n";
    right_node_->traversal();
  }
}
const int Treap_node::height() {
  int left_height = 0;
  if (left_node_) {
    left_height = left_node_->height();
  }
  int right_height = 0;
  if (right_node_) {
    right_height = right_node_->height();
  }
  return std::max(left_height, right_height) + 1;
}

const int Treap_node::get_key() {
  return key_;
}
const int Treap_node::get_priority() {
  return priority_;
}
std::shared_ptr<Treap_node> &Treap_node::get_left_node() {
  return left_node_;
}
std::shared_ptr<Treap_node> &Treap_node::get_right_node() {
  return right_node_;
}

void split(std::shared_ptr<Treap_node> root,
           const std::shared_ptr<Treap_node> split_node,
           std::shared_ptr<Treap_node> &left,
           std::shared_ptr<Treap_node> &right) {
  if (!root) {
    left = nullptr;
    right = nullptr;
  } else if (split_node->get_key() < root->get_key()) {
    split(root->get_left_node(), split_node, left, root->get_left_node());
    right = root;
  } else {
    split(root->get_right_node(), split_node, root->get_right_node(), right);
    left = root;
  }
}

void insert(std::shared_ptr<Treap_node> &root, std::shared_ptr<Treap_node> to_insert) {
  if (!root) {
    root = to_insert;
  } else if (to_insert->get_priority() > root->get_priority()) {
    split(root, to_insert, to_insert->get_left_node(), to_insert->get_left_node());
    root = to_insert;
  } else {
    if (to_insert->get_key() < root->get_key()) {
      insert(root->get_left_node(), to_insert);
    } else {
      insert(root->get_right_node(), to_insert);
    }
  }
}
