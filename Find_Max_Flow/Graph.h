//
// Created by liza on 12.11.16.
//
#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
#include <utility>

#ifndef FIND_MAX_FLOW_GRAPH_H
#define FIND_MAX_FLOW_GRAPH_H

class Graph {
private:
    size_t num_vertices_;
    std::vector<std::vector<size_t>> flow_;
    std::vector<std::vector<size_t>> capacity_;

    size_t source_;
    size_t target_;

    bool TryReach(size_t start, size_t target, std::vector<size_t> &parent);

public:
    const static int INF = 1000000000; // 1e9

    Graph();

    Graph(size_t num_vertices, size_t source, size_t target, std::vector<std::vector<size_t>> &&flow,
          std::vector<std::vector<size_t>> &&capacity);

    friend std::istream &operator>>(std::istream &in, Graph &our_graph);

    int find_max_flow();
};

#endif //FIND_MAX_FLOW_GRAPH_H
