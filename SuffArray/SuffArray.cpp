#include <cmath>
#include <utility>
#include <algorithm>
#include "SuffArray.h"

struct ClassPair {
  size_t first_class;
  size_t second_class;
  size_t pos;

  ClassPair(size_t first_class_, size_t second_class_, size_t pos_) {
    first_class = first_class_;
    second_class = second_class_;
    pos = pos_;
  }

  const bool operator<(const ClassPair &other) {
    return (first_class < other.first_class) ||
        (first_class == other.first_class && second_class < other.second_class) ||
        (first_class == other.first_class && second_class == other.second_class && pos < other.pos);
  }
};

SuffArray::SuffArray(const std::string &s) : string_(s + GUARANTEED_TO_BE_LESS_THEN_OTHER_SYMBOLS) {
  order_.resize(string_.length());
  equivalence_classes_.resize(Log2RoundUp(s.length()) + 1);

  for (auto &subvector: equivalence_classes_) {
    subvector.resize(s.length() + 1);
  }

  // PHASE 0
  std::vector<std::pair<char, size_t> > for_sort;
  for (size_t i = 0; i < string_.length(); ++i) {
    for_sort.push_back({string_[i], i});
  }
  std::sort(for_sort.begin(), for_sort.end());

  size_t cur_class = 0;
  equivalence_classes_[0][for_sort[0].second] = cur_class;

  for (size_t i = 1; i < for_sort.size(); i++) {
    if (string_[for_sort[i].second] != string_[for_sort[i - 1].second]) {
      cur_class++;
    }
    equivalence_classes_[0][for_sort[i].second] = cur_class;
  }

  // PHASE k
  for (size_t phase = 1; phase < equivalence_classes_.size(); ++phase) {
    int cur_shift = 1 << (phase - 1);

    std::vector<ClassPair> pending_sort;
    for (size_t i = 0; i < string_.length(); ++i) {
      pending_sort.push_back(ClassPair(equivalence_classes_[phase - 1][i],
                                       equivalence_classes_[phase - 1][(i + cur_shift) % string_.length()],
                                       i));
    }

    std::sort(pending_sort.begin(), pending_sort.end());

    for (size_t i = 0; i < string_.length(); ++i) {
      order_[i] = pending_sort[i].pos;
    }

    cur_class = 0;
    equivalence_classes_[phase][order_[0]] = cur_class;
    for (size_t i = 1; i < string_.length(); ++i) {
      if (pending_sort[i].first_class != pending_sort[i - 1].first_class ||
          pending_sort[i].second_class != pending_sort[i - 1].second_class) {
        cur_class++;
      }

      equivalence_classes_[phase][order_[i]] = cur_class;
    }
  }
}

size_t SuffArray::getLongestCommonPrefixLength(size_t first_suffix_pos, size_t second_suffix_pos) {
  size_t ans = 0;

  for (int phase = equivalence_classes_.size() - 1; phase >= 0; --phase) {
    if (equivalence_classes_[phase][first_suffix_pos] == equivalence_classes_[phase][second_suffix_pos]) {
      int shift = 1 << phase;
      ans += shift;
      first_suffix_pos += shift;
      second_suffix_pos += shift;
    }
  }

  return ans;
}

long long SuffArray::DistinctSubstringsNum() {
  long long result = static_cast<long long> ((string_.length()) * (string_.length() - 1) / 2);
  for (size_t i = 0; i < order_.size() - 1; ++i) {
    result -= getLongestCommonPrefixLength(order_[i], order_[i + 1]);
  }

  return result;
}

size_t SuffArray::Log2RoundUp(size_t length) {
  return static_cast<size_t>(ceil(log2(length)));
}
