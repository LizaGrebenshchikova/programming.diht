#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include "SuffArray.h"

int main() {
  std::string s;
  std::cin >> s;
  SuffArray suffArray = SuffArray(s);
  std::cout << suffArray.DistinctSubstringsNum();

  return 0;
}