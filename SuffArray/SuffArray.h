//
// Created by liza on 04.12.16.
//

#include <string>
#include <vector>

#ifndef SUFFARRAY_SUFFARRAY_H
#define SUFFARRAY_SUFFARRAY_H

class SuffArray {
 public:
  SuffArray(const std::string &s);
  size_t getLongestCommonPrefixLength(size_t first_prefix_pos, size_t second_prefix_pos);
  long long DistinctSubstringsNum();

 private:
  static const char GUARANTEED_TO_BE_LESS_THEN_OTHER_SYMBOLS = '!';
  static const char ALPHABET_SIZE = 26;

  std::string string_;
  std::vector<size_t> order_;

  /*
   * equivalence classes for each step of suffix array constructing
   */
  std::vector<std::vector<size_t> > equivalence_classes_;

  size_t Log2RoundUp(unsigned long length);
};

#endif //SUFFARRAY_SUFFARRAY_H
